import config from '../../config';

let logger;

if (config.env === 'test' || config.env === 'local' || config.env === 'development') {
	logger = console;
}

export default logger;
