import logger from '../utils/logger';

import User from '../models/user';

const ObjectId = require('mongodb').ObjectID;

exports.list = (req, res) => {
	User.find({})
		.then(users => {
			res.json(users);
		})
		.catch(err => {
			logger.error(err);
			res.status(404).send(err.errors);
		});
};

exports.listforsingleuser = (req, res) => {
	User.findOne({userName: req.params.username})
		.then(users => {
			res.json(users);
		})
		.catch(err => {
			logger.error(err);
			res.status(404).send(err.errors);
		});
};

exports.validatesingleuser = (req, res) => {
	User.findOne({userName: req.params.username, password: req.params.password})
		.then(users => {
			res.json(users);
		})
		.catch(err => {
			logger.error(err);
			res.status(404).send(err.errors);
		});
}

exports.post = (req, res) => {
	const data = Object.assign({}, req.body) || {};
	User.create(data)
		.then(result => {
			res.json(result);
		})
		.catch(err => {
			logger.error(err);
			res.status(500).send(err);
		});
};