import Card from '../models/card';
import logger from '../utils/logger';
exports.list = (req, res) => {
	Card.find()
		.then(cards => {
			res.json(cards);
		})
		.catch(err => {
			logger.error(err);
			res.status(404).send(err.errors);
		});
};

exports.listforsingleuser = (req, res) => {
	Card.find({userName: req.params.username})
		.then(cards => {
			cards.sort(function(a, b) {return b.createdAt - a.createdAt});
			res.json(cards);
		})
		.catch(err => {
			logger.error(err);
			res.status(404).send(err.errors);
		});
};

exports.checkduplicate = (req, res) => {
	Card.findOne({cardNumber: req.params.cardnumber, userName: req.params.username}).then(cards => {
		if (cards === undefined || cards === null || cards.length === 0) {
			res.json({duplicateRecord: false})
		} else {
			res.json({duplicateRecord: true})
		}
	}).catch(err => {
		logger.error(err);
		res.status(404).send(err.errors);
	});
}

exports.post = (req, res) => {
	const data = Object.assign({}, req.body) || {};
	Card.create(data)
		.then(result => {
			res.json(result);
		})
		.catch(err => {
			logger.error(err);
			res.status(500).send(err);
		});
};