import logger from '../utils/logger';

import Transaction from '../models/transaction';
import User from '../models/user';

const ObjectId = require('mongodb').ObjectID;

exports.list = (req, res) => {
	Transaction.find({})
		.then(transactions => {
			res.json(transactions);
		})
		.catch(err => {
			logger.error(err);
			res.status(404).send(err.errors);
		});
};

exports.listforsingleuser = (req, res) => {
	Transaction.find({userName: req.params.username})
		.then(transactions => {
			getTransactionsResponse(transactions).then(data => {
				data.sort(function(a, b) {return b.createdAt - a.createdAt});
				res.json(data);
			});
		})
		.catch(err => {
			logger.error(err);
			res.status(404).send(err.errors);
		});
};

exports.getbalance = (req, res)=> {
	Transaction.find({userName: req.params.username})
		.then(transactions => {
            res.json({balance: getTotalBalance(transactions)});
		})
		.catch(err => {
			logger.error(err);
			res.status(404).send(err.errors);
		});
};

const getTotalBalance = (transactions) => {
    return makesum(transactions.filter(function(item) {return item.transType === 'RECEIVE'}), 'amount')
   - makesum(transactions.filter(function(item) {return item.transType === 'SEND'}), 'amount');
}
const makesum = function(items, prop){
    return items.reduce( function(a, b){
        return a + b[prop];
    }, 0);
};
exports.post = (req, res) => {
	const data = Object.assign({}, req.body) || {};
	Transaction.create(data)
		.then(result => {
			getTranactionResponse(result)
				.then( finaldata => res.json(finaldata));
		})
		.catch(err => {
			logger.error(err);
			res.status(500).send(err);
		});
};

exports.transfer = async(req, res) => {
	const data = Object.assign({}, req.body) || {};
	Transaction.create(data[0]).then(
	 firstRes =>	
		{	
			Transaction.create(data[1]).then(
				secondRes => {
						getTranactionResponse(data[0])
						.then( finaldata => res.json(finaldata));
				}
			)
		}
	)	
};

const getTranactionResponse = (transaction) => {
	return new Promise((resolve, reject) => {
		User.findOne({"userName": transaction.partner}).then(
			user => {
				let newTx = {};
				newTx.username = transaction.userName;
				newTx.partnerName = user.fullName;
				newTx.amount = transaction.amount;
				newTx.date = formatDate(transaction.date);
				newTx.time = formatTime(transaction.time);
				resolve(newTx);
			}).catch(err => {
				logger.error(err);
				reject(err);
			});
	});
}

const getTransactionsResponse = (transactions) => {
	return new Promise((resolve, reject) => {
	let finalRes = [];
	let count = 0; let length = transactions.length;
 	transactions.forEach( (item) => {
		getTranactionResponseForList(item).then(
			res => {finalRes.push(res);
				count++;
				if (count === length)
				{
					resolve(finalRes);
				} 
			}
		)})
	});
}

 const getTranactionResponseForList = transaction => {
	return new Promise((resolve, reject) => {
	User.findOne({"userName": transaction.partner}).then(
		user => {
			let newTx = {};
			newTx.amount = transaction.transType === 'SEND' ? (-1) * transaction.amount : transaction.amount;
			newTx.userName = transaction.userName;
			newTx.date = formatDate(transaction.date);
			newTx.time = formatTime(transaction.time);
			newTx.partnerName = user.fullName;
			newTx.createdAt = transaction.createdAt;
			resolve(newTx);
		}).catch(err => {
			logger.error(err);
			reject(err);
		});
	});
}; 

const formatDate = (currentDate) => {
	let monthNames = [
		"January", "February", "March",
		"April", "May", "June", "July",
		"August", "September", "October",
		"November", "December"
	  ];
	let date = new Date(currentDate);
	let day = date.getDate(currentDate);
	let monthIndex = date.getMonth();
	let year = date.getFullYear();
	
	return monthNames[monthIndex] + ' ' + day + ' ' + year;
}

const formatTime = (currentTime) => {
	let res = "";
	if (currentTime === undefined) {
		return '12:00 PM';
	}
	const hour = currentTime.split(':')[0];
	const min = currentTime.split(':')[1];
	res = hour > 12 ? hour - 12 + ':' + min + ' PM' : currentTime + ' AM';
	return res;
}
