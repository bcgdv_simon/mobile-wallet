const userController = require('../controllers/user');
const express = require('express');
const multer = require('multer');


const upload = multer();

const router = express.Router();
router.post('/', upload.none(), userController.post);
router.get('/user/:username', userController.listforsingleuser);
router.get('/user/:username/:password', userController.validatesingleuser);
router.get('/', userController.list);
module.exports = router;