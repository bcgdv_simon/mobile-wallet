const transactionController = require('../controllers/transaction');
const express = require('express');
const multer = require('multer');


const upload = multer();

const router = express.Router();
router.post('/', upload.none(), transactionController.post);
router.get('/user/:username', transactionController.listforsingleuser);
router.get('/user/:username/balance', transactionController.getbalance);
router.post('/transfer', upload.none(), transactionController.transfer);

module.exports = router;