const cardController = require('../controllers/card');
const express = require('express');
const multer = require('multer');


const upload = multer();

const router = express.Router();
router.post('/', upload.none(), cardController.post);
router.get('/user/:username', cardController.listforsingleuser);
router.get('/user/:username/card/:cardnumber', cardController.checkduplicate);

module.exports = router;