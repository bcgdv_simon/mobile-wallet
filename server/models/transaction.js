import mongoose, { Schema } from 'mongoose';
import timestamps from 'mongoose-timestamp';
import mongooseStringQuery from 'mongoose-string-query';

export const TransactionSchema = new Schema(
    {
        userName: {
            type: String,
            required: true,
            trim: true
        },
        partner: {
            type: String,
            required: true,
            trim: true          
        },
        transType: {
            type: String,
            enum: ['SEND', 'RECEIVE'],
            required: true,
            trim: true
        },
        amount: {
            type: Number,
            required: true
        },
        date: {
            type: String,
            required: true
        },
        time: {
            type: String,
            required: true
        }
    },
    { collection: 'transactions' },
);

TransactionSchema.plugin(timestamps);
TransactionSchema.plugin(mongooseStringQuery);

module.exports = mongoose.model('Transaction', TransactionSchema);