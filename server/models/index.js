const mongoose = require('mongoose');

const Card = require('./card');
const Transaction = require('./transaction');
const User = require('./user');


module.exports.connectDb = connectionString =>
	mongoose.connect(connectionString, { useNewUrlParser: true });

module.exports.models = { Card, Transaction, User };