import mongoose, { Schema } from 'mongoose';
import timestamps from 'mongoose-timestamp';
import mongooseStringQuery from 'mongoose-string-query';

export const CardSchema = new Schema(
    {
        userName: {
            type: String,
            required: true,
            trim: true
        },
        cardName: {
            type: String,
            required: true,
            trim: true,
            unique: true     
        },
        cardNumber: {
            type: String,
            required: true,
            trim: true
        },
        expirationDate: {
            type: String,
            required: true,
            trim: true
        },
        cvvCode: {
            type: String,
            required: true
        }
    },
    { collection: 'cards' },
);

CardSchema.plugin(timestamps);
CardSchema.plugin(mongooseStringQuery);

module.exports = mongoose.model('Card', CardSchema);