import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import compression from 'compression';
import 'core-js/stable';
import 'regenerator-runtime/runtime';
import config from './config';
// app.js
const api = express();
api.use(cors());
api.use(compression());
api.use(bodyParser.urlencoded({ extended: true }));
api.use(bodyParser.json());

const user = require('./routes/user');
const transaction = require('./routes/transaction');
const card = require('./routes/card');

api.use('/users', user );
api.use('/transactions', transaction);
api.use('/cards', card);

api.listen(config.server.port, () => {
    require('./utils/db');
    console.log(`API is now running on port ${config.server.port} in ${config.env} mode`);
});

module.exports = api;