require('dotenv').config({ path: './.env' });

export default {
	env: process.env.NODE_ENV || 'development',
	server: {
		port: process.env.PORT || 1234,
	},
	logger: {
		host: process.env.LOGGER_HOST,
		port: process.env.LOGGER_PORT,
	},
	database: {
		uri: process.env.DATABASE_URI,
	}
};
