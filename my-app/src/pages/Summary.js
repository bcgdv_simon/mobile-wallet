import React from 'react'
import HeaderMenu from '../components/HeaderMenu'
import BalanceBlock from '../components/BalanceBlock'
import TransactionDetail from '../components/TransactionDetail'
import axios from 'axios';
import {CONFIG} from '../constants/config';
class Summary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            balanceAmount: 2000,
            fullName: "test1",
            transdata: []
        }
        this.getUserBalance = this.getUserBalance.bind(this);
        this.getUserTransaction = this.getUserTransaction.bind(this);
    }

    componentDidMount () {
        this.getUserBalance();
        this.getUserTransaction();
    }

    getUserTransaction() {
        axios.get(`${CONFIG.apiHost}/transactions/user/${this.props.match.params.username}`).then(
            res => {
                this.setState({transdata: res.data});
            }
        )
    }
    getUserBalance() {
        axios.get(`${CONFIG.apiHost}/transactions/user/${this.props.match.params.username}/balance`).then(
            res => {
                if(res !== null && res.data.balance !== undefined) {
                    this.setState({balanceAmount: res.data.balance})
                }
            }
        ).catch((error) => {console.error(error)})
    }
    render() {
        return (
            <div >
                <HeaderMenu username={this.props.match.params.username} /> 
                <div className="summary-page">
                    <BalanceBlock username={this.props.match.params.username} />
                    <div className="summary-nav-tab-block">
                        <span className="summary-nav-tab-item-left" onClick={event => this.navTransferOut(event)}>Send</span>
                        <span className="summary-nav-tab-item" onClick={event => this.navLoadinWallet(event)}>Load Wallet</span>
                    </div>
                    <div className="segment">Activity</div>
                    <TransactionDetail username={this.props.match.params.username} />
            </div>
            </div>
        )
    }
    navLoadinWallet(event) {
        window.location.assign(`/load_wallet/${this.props.match.params.username}`)
    }

    navTransferOut(event) {
        window.location.assign(`/transfer_money/${this.props.match.params.username}`)
    }
}

export default Summary