import React from 'react';
import axios from 'axios';
import {CONFIG} from '../constants/config';
import Logo from "../resources/images/PagoPop_Logo.png";
import InputMask from "react-input-mask";
class SignIn extends React.Component {
    constructor(props) {
        super(props);
        this.signIn = this.signIn.bind(this);
        this.handleChangeForUsername = this.handleChangeForUsername.bind(this);
        this.handleChangeForPassword = this.handleChangeForPassword.bind(this);
        this.signIn = this.signIn.bind(this);
        this.state = {
          balanceAmount: 2000,
          username: "",
          password: "",
          errormessage: ""
      }
    }
    componentDidMount () {
      this.setState({errormessage:""});
  }
    render() {
        return (
          <div className="signin-page-block">
            <div >
            <img src={Logo} alt='website logo'  className="logo-image" />
            </div>     
            <div className="signinblock">
              <div className="error-message">{this.state.errormessage}</div>
              <div>
              <InputMask className="signin-input" size="32" mask="9999999999" placeholder="Your Phone Number" value={this.state.username} onChange={this.handleChangeForUsername}></InputMask>
              <input className="signin-input" type="password" name="sign_in_password" size="32" placeholder="Your Password" value={this.state.password} onChange={this.handleChangeForPassword}></input>
              </div>
              <button className='signin-button' onClick={event => {this.signIn(event)}}>Sign In</button>
            </div>
            </div>
        )
    }
    signIn() { 
      axios.get(`${CONFIG.apiHost}/users/user/${this.normalizeNumber(this.state.username)}/${this.state.password}`).then(
        res => {
          if (res.data !== null) {
            this.setState({errormessage:""});
            window.location.assign(`/summary/${this.normalizeNumber(this.state.username)}`)
          }
          else {
            this.setState({errormessage :"Username or password is invalid."});
          }
        }
      ).catch( error => {console.error("Cannot login.", error)}) 
    }

    normalizeNumber(input) {
      return parseInt(input.replace(/[^0-9]/g, ''));
    }

    handleChangeForUsername(event) {
      this.setState({username: event.target.value});
    }
    handleChangeForPassword(event) {
      this.setState({password: event.target.value});
    }
}

export default SignIn