import React from 'react';
import axios from 'axios';
import {CONFIG} from '../constants/config';
import Contacts from '../components/Contacts';
import SendMoney from '../components/SendMoney';
import SendMoneyConfirmation from '../components/SendMoneyConfirmation';

class TransferMoney extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userList: [],
            transaction:{},
            selectedItemUserName: "",
            selectedItemFullName: "",
            showContactItem:"show-item",
            showSendMoney:"hide-item",
            showConfirmation:"hide-item",
        };
    }
    componentDidMount () {
        this.getUserInfo(this.props.match.params.username);
    }
    handleSelectContact = (userName, fullName) => {
        this.setState({selectedItemUserName: userName});
        this.setState({selectedItemFullName: fullName});
        this.setState({showContactItem: "hide-item"});
        this.setState({showSendMoney: "show-item"});
        this.setState({showConfirmation: "hide-item"})
    }
    handleSendMoney = (contactUsername ,inputAmount) => {
        this.loadAmount(contactUsername, inputAmount);
        this.setState({showContactItem: "hide-item"});
        this.setState({showSendMoney: "hide-item"});
        this.setState({showConfirmation: "show-item"});
    }
    handleCancelAction = () => {
        this.setState({showContactItem: "show-item"});
        this.setState({showSendMoney: "hide-item"});
        this.setState({showConfirmation: "hide-item"});
    }
    handleConfirmation = (username) => {
        window.location.assign(`/summary/${username}`);
    }
    render() {
        return (
            <div>
                <div className={this.state.showContactItem}>
                    <Contacts username={this.props.match.params.username}
                              onSelectContact= {this.handleSelectContact}
                    />
                </div>
                <div className={this.state.showSendMoney}>
                    <SendMoney  username={this.props.match.params.username}
                                contactUserName={this.state.selectedItemUserName}
                                contactFullName={this.state.selectedItemFullName} 
                                onSendMoney={this.handleSendMoney}
                                onCancelAction = {this.handleCancelAction} 
                    />
                </div>
                <div className={this.state.showConfirmation}>
                    <SendMoneyConfirmation 
                        username={this.props.match.params.username}
                        partnerName={this.state.transaction.partnerName}  
                        amount={this.state.transaction.amount}
                        date={this.state.transaction.date}
                        time={this.state.transaction.time}               
                    onConfirmation={this.handleConfirmation}
                    />
                </div>
                
            </div>
        )
    }
    
    getUserInfo(username) {
        axios.get(`${CONFIG.apiHost}/users`).then(
            response => {
                this.setState({userList: response.data.filter(function(item) {return item.userName !== username})});
                this.setState({selectedCardItem: this.state.userList[0].userName})})
    }

    loadAmount(contactUsername, inputAmount) {
        const currentTimeStamp = new Date();
        const data = [];
        data.push( {userName:this.props.match.params.username, partner: contactUsername, transType: 'SEND', date: this.getCurrentDate(currentTimeStamp), time: this.getCurrentTime(currentTimeStamp), amount: inputAmount});
        data.push({userName: contactUsername, partner: this.props.match.params.username, transType: 'RECEIVE', date: this.getCurrentDate(currentTimeStamp), time: this.getCurrentTime(currentTimeStamp), amount: inputAmount});
        axios.post(`${CONFIG.apiHost}/transactions/transfer`,data).then(
           res => {
               this.setState({transaction: res.data})}
        )
    }

    getCurrentDate(currentTimeStamp) {
        const date = `${currentTimeStamp.getFullYear()}/${(currentTimeStamp.getMonth() + 1)}/${(currentTimeStamp.getDate())}`;
        return date;
    }

    getCurrentTime(currentTimeStamp) {
        const time = `${currentTimeStamp.getHours()}:${(currentTimeStamp.getMinutes())}`;
        return time;
    }
}

export default TransferMoney