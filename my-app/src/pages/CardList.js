import React from 'react'
import axios from 'axios'
import {CONFIG} from '../constants/config'
import InputMask from "react-input-mask";
import Card from '../components/Card';
class CardList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "test1",
            title: "Credit Card List",
            addCardFormClass: "hide-item",
            cardlistClass: "list-cards",
            cardName: '',
            dueDate: '',
            cvvCode: '',
            cardNumber: '',
            cardList: [],
            errorMessage: ''
        }
    }
    componentDidMount () {
        this.setState({username: this.props.match.params.username});
        this.getCardInfo();
    }
    render() {
        return (
            <div>
            <div className={this.state.cardlistClass}>
             <div className="link-card-title">My Cards</div>
             <div className="existing-cards">
                { this.renderCardInfo(this.state.cardList) }
             </div>
            <div className='add-card-button' onClick={event => this.addCard()}>+</div>
            <div className='card-back-home' onClick={event => this.backToHome()}>Home</div>
            </div>
            
            <div className={this.state.addCardFormClass} >
             <div className="link-card-title">Link your new card</div>
             <div className="error-message">{this.state.errorMessage}</div>
             <div className="link-card-subtitle">Provide your card information to link it to your PagoPop account</div>
             <div className="add-form-block"> 
    
             <div className="add-form-row"> <InputMask id="card-number" placeholder="Card Number" value={this.state.cardNumber} mask="9999 9999 9999 9999" onChange={event => this.handleChangeCardNumber(event)} className="add-card-form-field" size="32" /></div>  
            
             <div className="add-form-row"> <InputMask id="card-due-date" placeholder="Due Date (MM/YY)" value={this.state.dueDate} mask="99/99" onChange={event => this.handleChangeCardDueDate(event)} className="add-card-form-field" size="32" /></div>        
             <div className="add-form-row"><input id="card-holder-name" placeholder="Name on Card" value={this.state.cardName} onChange= {event => this.handleChangeCardName(event)} size="32" className="add-card-form-field" /></div>       
              
              <div className="add-form-row"> <InputMask  id="card-cvv" mask="9999" value={this.state.cvvCode} onChange={event => this.handleChangeCVVCode(event)} className="add-card-form-field" size="32" placeholder="CVV"/></div>
              </div>
              <div className="card-buttons">
              <div className='cancel-card-button' onClick={event => this.cancelCard()}>Cancel</div>
              <div className='save-card-button' onClick={event => {
                    this.handleSaveCardEvent(event);                  
                  }}>Save</div>
              </div>
            </div>
            
            </div>
        )
    }
    handleSaveCardEvent(event) {
          if (this.normalizeNumber(this.state.cardNumber).toString().length === 16 
          && this.normalizeNumber(this.state.cvvCode).toString().length >= 3 
          && this.normalizeNumber(this.state.dueDate).toString().length === 4
          && this.state.cardName.length > 0) 
          {     
              if (this.validateDueDate() === true) {
                axios.get(`${CONFIG.apiHost}/cards/user/${this.state.username}/card/${this.normalizeNumber(this.state.cardNumber)}`).then(
                    res => {
                        if (res.data.duplicateRecord === true) {
                         this.setState({errorMessage: 'Card cannot be saved since it is already been saved.'});
                        }
                        else {
                            this.saveCard();
                        }
                    }
                )
              }
              else {
                this.setState({errorMessage: 'Card Due Date is invalid.'});
              }
          }
          else {
              this.setState({errorMessage: 'Card Information is incomplete.'});
          }
    }
    validateDueDate() {
       const data = this.state.dueDate.split('/');
       const cardMonth = parseInt(data[0]);
       const cardYear = parseInt(data[1]);
       const currentDate = new Date();
       const year = currentDate.getFullYear().toString().substr(2,2);
       if (cardMonth <= 12 && cardMonth > 0 && cardYear >= parseInt(year)) {
           return true;
       }
       else {
           return false;
       }
    }
    backToHome() {
        window.location.assign(`/summary/${this.props.match.params.username}`);
    }
    handleChangeCardName(event) {
        this.setState({cardName: event.target.value})      
    }
    handleChangeCVVCode(event) {
        this.setState({cvvCode: event.target.value})
    }
    
    handleChangeCardNumber(event) {
        this.setState({cardNumber: event.target.value})
    }
    handleChangeCardDueDate(event) {
        this.setState({dueDate: event.target.value})
    }
    addCard() {
    this.setState({errorMessage: ''});
    this.setState({cardName: ''});
    this.setState({dueDate: ''});
    this.setState({cvvCode: ''});
    this.setState({cardNumber: ''});
    this.setState({addCardFormClass: "list-cards"});
    this.setState({cardlistClass: "hide-item"});
    this.setState({title: "Add Credit Card"})
    }
    cancelCard() {
        this.setState({addCardFormClass: "hide-item"});
        this.setState({cardlistClass: "list-cards"});     
    }
    saveCard() {
       //check card is already been saved or not.
       axios.post(`${CONFIG.apiHost}/cards`, {userName: this.state.username, cardName: this.state.cardName, cardNumber: this.normalizeNumber(this.state.cardNumber), expirationDate: `${this.state.dueDate}`, cvvCode: this.normalizeNumber(this.state.cvvCode)})
       .then(response => {this.getCardInfo();}).catch(error => {
           console.error("error message: ", error)
           this.setState({errorMessage: 'Card cannot be saved due to technical difficulties.'});
        })
       this.setState({addCardFormClass: "hide-item"});
       this.setState({cardlistClass: "list-cards"});
    }
    getCardInfo() {
        axios.get(`${CONFIG.apiHost}/cards/user/${this.props.match.params.username}`).then(
            response => {
                this.setState({cardList: response.data});
            }
        ).catch(error => {
            console.error(error);
        })
    }
    normalizeNumber(input) {
        return parseInt(input.replace(/[^0-9]/g, ''));
    }
    renderTableHeader() {
        let header = ["Name", "Card Number", "EXP DATE", "CVV"];
        return header.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        }) 
    }
    applyMask(card_number) {
        let len = card_number.length;
        return "**** " + card_number.substr(len - 4, len);
    }
    renderCardInfo(cards) {
    return cards.map((item, index) => {
        return (
            <Card key={index} cardNumber={this.applyMask(item.cardNumber)} />
        )
    })}
}
        


export default CardList