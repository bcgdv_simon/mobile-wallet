import React from 'react';
import axios from 'axios';
import {CONFIG} from '../constants/config';
import InputMask from "react-input-mask";
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import Checkmark from "../resources/images/checkmark.svg";
class LoadWallet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "test1",
            cardlist: [],
            receivedmoney: "",
            selectedOption: null,
            loadWalletStyle: "load-wallet-block",
            loadConfirmationStyle: "hide-item",
            balanceAmount: 0,
            loadAmount: 0,
            loadAmountTime: '',
            loadAmountDate: '',
            errormessage:""
        }
    }
    componentDidMount () {
        this.loadCardInfo();
    }

    loadAmount() {
        //add new transaction
        const currentTimeStamp = new Date();
        axios.post(`${CONFIG.apiHost}/transactions`,
        {   userName:this.props.match.params.username,
            partner: this.props.match.params.username,
            transType: 'RECEIVE',
            date: this.getCurrentDate(currentTimeStamp),
            time: this.getCurrentTime(currentTimeStamp),
            amount: this.normalizeNumber(this.state.receivedmoney)}).then(
            res => {
                console.log("load money is successful.");
                console.log("data", res.data);
                this.renderConfirmationPage(res.data.username, res.data);

                //window.location.assign(`/summary/${this.props.match.params.username}`);
            }
        ).catch(error => {console.error(error)})
    }

    renderConfirmationPage(username, data) {
        const URL = `${CONFIG.apiHost}/transactions/user/${username}/balance`
        axios.get(URL).then(
            res => {
                if(res !== null && res.data.balance !== undefined) {
                    this.setState({balanceAmount: res.data.balance})
                    this.setState({loadAmount: data.amount})
                    this.setState({loadAmountDate: data.date})
                    this.setState({loadAmountTime: data.time})
                    this.setState({username: username})
                    this.setState({loadWalletStyle: "hide-item"})
                    this.setState({loadConfirmationStyle: "load-wallet-block"})
                }
            }
        ).catch((error) => {throw error;})
    }
    getCurrentDate(currentTimeStamp) {
        const date = `${currentTimeStamp.getFullYear()}/${(currentTimeStamp.getMonth() + 1)}/${(currentTimeStamp.getDate())}`;
        return date;
    }

    getCurrentTime(currentTimeStamp) {
        const time = `${currentTimeStamp.getHours()}:${(currentTimeStamp.getMinutes())}`;
        return time;
    }

    loadCardInfo() {
        axios.get(`${CONFIG.apiHost}/cards/user/${this.props.match.params.username}`).then( res => {
            this.setState({cardlist: this.getMaskedData(res.data)})
        } )
        .catch( error => {
            console.error("error: ", error);
        })
    }
    render() {
        return (
           <div> 
                <div className={this.state.loadWalletStyle}>
                        <div className='load-wallet-title'>Fund My Account</div>
                        <div className="error-message">{this.state.errormessage}</div>
                        <div className='load-wallet-subtitle'>How much money do you want to pay to your account?</div>
                        <div className='load-wallet-instruction'>Enter the amount you wish to pay to your account</div>
                        <div className="load-wallet-form">
                            <div className="load-wallet-money">
                        <span className="load-wallet-currency">$</span>
                        <span><InputMask className="load-wallet-value" mask="999999" name="load_money"
                        value={this.state.receivedmoney} 
                        onChange={event => this.handleChange(event)}></InputMask></span> 
                            </div>
                            <div>                      
                            <Dropdown         
                            value={this.state.selectedOption}
                            onChange={this.handleSelectChange}
                            options={this.state.cardlist} 
                            placeholder="select a card" />
                            </div> 
                    
                        </div>
                        <div className="card-buttons">
                            <div className='cancel-card-button' onClick={event => this.backToHome()}>Home</div>
                            <div className='save-card-button' onClick={event => { 
                                if (this.state.receivedmoney !== '' 
                                    && this.normalizeNumber(this.state.receivedmoney) !== ''
                                    && this.normalizeNumber(this.state.receivedmoney) !== 0
                                    && this.state.selectedOption !== null) 
                                    {
                                        this.setState({errormessage:""});
                                        this.loadAmount();
                                    }
                                else if (this.state.selectedOption == null) {
                                    this.setState({errormessage: "Card is required."});
                                }
                                else {
                                    this.setState({errormessage: "Amount is required and must be positive."})
                                }
                                }}>Load</div>
                        </div> 
                </div>
                <div className={this.state.loadConfirmationStyle}>
                <div className="sendmoney-confirmation-block">
                <div className="sendmoney-conf-content">
                    <div className="avatar-style">
                        <img src={Checkmark} alt='checkmark' className="checkmark-image" />
                    </div>
                <div className="sendmoney-conf-msg">Confirmed</div>
                <div className="sendmoney-conf-amt">
                    <span className="sendmoney-conf-currency">$</span>
                    <span className="sendmoney-conf-value">{this.state.loadAmount}</span>
                </div>
                <div className="load-money-timestamp">
                    <div className="sendmoney-conf-date">{this.state.loadAmountDate}</div>
                    <div className="sendmoney-conf-time">{this.state.loadAmountTime}</div>
                </div>
                <div className="load-money-balance">
                    Your current balance is:
                </div>
                <div className="load-money-balance-amount">
                    ${this.state.balanceAmount}
                </div>
                <div className="back-to-home" onClick={event => {this.backToHome()}}>Home</div>
                </div>
            </div>
                </div>
            </div>
        )
    }

    normalizeNumber(input) {
        return parseInt(input.replace(/[^0-9]/g, ''));
      }

    handleSelectChange = selectedOption => {
        this.setState(
            { selectedOption }
          ); 
    }
    backToHome() {
        window.location.assign(`/summary/${this.props.match.params.username}`);
    }
    getMaskedData(arr) {
        let result = [];
        arr.map((item, index) => {
           result.push(this.applyMask(item.cardNumber));
        });
        return result;
    }
    handleChange(event) {
        this.setState({receivedmoney: event.target.value});
      }

    applyMask(card_number) {
        let len = card_number.length;
        return {value: `**** ${card_number.substr(len - 4, len)}`, lable: `**** ${card_number.substr(len - 4, len)}`};
    }
}

export default LoadWallet