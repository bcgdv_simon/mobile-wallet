import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Switch, BrowserRouter as Router, Redirect } from 'react-router-dom'
import './index.css';
import * as serviceWorker from './serviceWorker';
import SignIn from './pages/SignIn';
import CardList from './pages/CardList';
import LoadWallet from './pages/LoadWallet';
import Summary from './pages/Summary';
import TransferMoney from './pages/TransferMoney';
import App from './App';

const routing = (
    <Router>
        <Switch>
            <Route path="/login" component={SignIn} />
            <Route path="/cards/:username" component={CardList} />
            <Route path="/load_wallet/:username" component={LoadWallet} />
            <Route path="/summary/:username" component={Summary} />
            <Route path="/transfer_money/:username" component={TransferMoney} />
            <Route render={() => <Redirect to={{pathname: "/login"}} />} />
        </Switch>
    </Router>
)

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
