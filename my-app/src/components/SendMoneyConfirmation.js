import React from 'react';
import Avatar from 'react-avatar'
import Checkmark from "../resources/images/checkmark.svg";
class SendMoneyConfirmation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            transactionDate: "",
            transactionTime: "",
            receiverFullName: "",
            amount: ""
        }
    }
    componentDidMount () {
    }

    render() {
        return (
            <div className="sendmoney-confirmation-block">
                <div className="sendmoney-conf-content">
                <div className="avatar-style">
                    <img src={Checkmark} alt='website logo' className="checkmark-image" />
                </div>
                <div className="sendmoney-conf-msg">Confirmed</div>
                <div className="sendmoney-conf-amt">
                    <span className="sendmoney-conf-currency">$</span>
                    <span className="sendmoney-conf-value">{this.props.amount}</span>
                </div>
                <div className="sendmoney-conf-send-msg">Automatically transferred to</div>
                <div className="sendmoney-conf-name">
                    <div className="sendmoney-conf-name-avatar"><Avatar name={this.props.partnerName} size="50" round="50px" /></div>
                    <div className="sendmoney-conf-name-fullname">{this.props.partnerName}</div>
                </div>
                <div className="sendmoney-conf-date">{this.props.date}</div>
                <div className="sendmoney-conf-time">{this.props.time}</div>
                <div className="back-to-home" onClick={event => this.props.onConfirmation(this.props.username)}>Home</div>
                </div>
            </div>
        )
    }

}

export default SendMoneyConfirmation