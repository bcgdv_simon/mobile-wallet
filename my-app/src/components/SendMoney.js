import React from 'react';
import axios from 'axios';
import {CONFIG} from '../constants/config';
import InputMask from "react-input-mask";
class SendMoney extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            transaction: {},
            balanceAmount: 0,
            inputAmount: "",
            errormessage:""
        }
        this.getUserBalance = this.getUserBalance.bind(this);
    }
    componentDidMount () {
        this.getUserBalance();
    }

    getUserBalance() {
        const URL = `${CONFIG.apiHost}/transactions/user/${this.props.username}/balance`
        axios.get(URL).then(
            res => {
                if(res !== null && res.data.balance !== undefined) {
                    this.setState({balanceAmount: res.data.balance})
                }
            }
        ).catch((error) => {throw error;})
    }
    normalizeNumber(input) {
        return parseInt(input.replace(/[^0-9]/g, ''));
      }

    handleChangeAmount(event) {
        this.setState({inputAmount: event.target.value})
    }

    render() {
        return (
            <div className="contact-block">
                 <div className="transfer-out-page-header">Transfer Money</div>
                 <div className="error-message">{this.state.errormessage}</div>
                 <div className="tranfer-out-header">               
                    <div className="tranfer-out-header-title">Name</div>
                    <div className="tranfer-out-header-content">{this.props.contactFullName}</div>
                 </div>
                 <div className="transfer-out-content">
                     <div className="transfer-out-input">
                         <div className="transfer-out-content-title">Amount</div>

                         <div className="transfer-out-content-value">
                             <span className="transfer-out-currency">$</span>
                             <span className="transfer-out-amount">
                                 <InputMask  className="transfer-out-input" mask="999999" value={this.state.inputAmount}
                                 onChange={event => this.handleChangeAmount(event)} 
                                 ></InputMask></span>
                        </div> 
                         
                     </div>
                     <div className="transfer-out-balance">
                        <div className="transfer-out-content-title">Balance</div>
                        <div className="transfer-out-content-value">${this.state.balanceAmount}</div>   
                     </div>
                 </div>
                 <div className="transfer-out-footer">
                     <div className="transfer-out-back" 
                        onClick={event => this.props.onCancelAction()}>
                            Back</div>
                     <div className="transfer-out-send" onClick={event => 
                        {
                            if (this.normalizeNumber(this.state.inputAmount) !== "" && this.normalizeNumber(this.state.inputAmount) > 0) {
                                this.setState({errormessage: ""});
                                 this.props.onSendMoney(this.props.contactUserName, this.normalizeNumber(this.state.inputAmount))
                            }
                            else {
                                this.setState({errormessage: "Amount is required and must be positive"})
                            }
                        }
                        }>
                            Send</div>
                 </div>
            </div>
        )
    }

}

export default SendMoney