import React from 'react';
import Avatar from 'react-avatar'
import Icon from '@material-ui/core/Icon';
class Contact extends React.Component {

    handleClick = () => this.props.onClick(this.props.index, this.props.item)

    render() {
      return  <div className="single-contact" 
      onClick= { this.handleClick }>
         <div className="contact-icon">
         <Avatar name={this.props.item.fullName} size="40" round="20px" />
         </div>
         <div className="contact-detail">
           {this.props.item.fullName}
         </div>
         <div className="contact-checkbox-show" >
             <Icon className={this.props.isActive ? "contact-select-style-show" : "contact-select-style-hide"}>done</Icon>
         </div>                
     </div>
    }
  }


  export default Contact