import React from 'react';
import axios from 'axios';
import {CONFIG} from '../constants/config';
import Contact from '../components/Contact';
class Contacts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contacts: [],
            selectedContactFullName:"",
            selectedContactUserName:"",
            continueStyle: "contact-continue-disable",
            couldContinue: false,
            headerText: "Who do you want to send this to?",
            contactCheckboxStyle: "contact-checkbox-hide"
        }
        this.getContacts = this.getContacts.bind(this);
    }
    componentDidMount () {
        this.getContacts();
    }
    getContacts() {
        const URL = `${CONFIG.apiHost}/users`
        axios.get(URL).then(
            res => {
                if(res !== null && res.data !== undefined) {
                    this.setState({contacts: res.data})
                }
            }
        ).catch((error) => {throw error;})
    }

    handleClick = (index, item) => {
        this.setState({ activeIndex: index });
        this.setState({selectedContactUserName: item.userName});
        this.setState({selectedContactFullName: item.fullName});
        this.setState({continueStyle: "contact-continue-enable"});
    }

    renderContactList(contacts) {
        return contacts.map((item, index) => {
                if (item.userName !== this.props.username) {
                    return (
                    <Contact key={ index }
                    index = { index }
                    item={ item }
                    onClick={ this.handleClick }
                    isActive={ this.state.activeIndex === index }
                    />
                    )
                }
            })
    }
    backToHome() {
        window.location.assign(`/summary/${this.props.username}`);
    }
    render() {
        return (
            <div className="contact-block">
                 <div className="transfer-money-header-init">{this.state.headerText}</div>
                <div className="contact-placeholder"></div> {/* TODO: Search A Name Implementation */}
                <div className="contact-title">Your Contacts</div>
                {this.renderContactList(this.state.contacts)}
                <div className="bottom-navigation">
                <div className="bottom-navigation-back-home" onClick={
                    event => this.backToHome()
                }>Home</div>
                <div className={this.state.continueStyle} 
                     onClick={event =>
                        {
                            if (this.state.continueStyle === "contact-continue-enable") {
                                this.props.onSelectContact(this.state.selectedContactUserName, this.state.selectedContactFullName)
                            }
                      }}>
                     Continue
                </div>
                </div>
            </div>
        )
    }

}

export default Contacts