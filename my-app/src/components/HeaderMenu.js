import React from 'react'
import { slide as Menu } from 'react-burger-menu';
import Avatar from 'react-avatar'
import axios from 'axios';
import {CONFIG} from '../constants/config';
import Logo from "../resources/images/PagoPop_Logo.png";

class HeaderMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fullName: ""
        }
        this.getUserProfile = this.getUserProfile.bind(this);
    }
    componentDidMount () {
        this.getUserProfile();
    }
    getUserProfile() {
         axios.get(`${CONFIG.apiHost}/users/user/${this.props.username}`).then(
            res => {
                if (res.data != null) {
                    this.setState({fullName: res.data.fullName});
                }
            }).catch(error => {
                console.error("error ocurred: ", error);
            });
    }
    render() {
        return (
            <div className="menu-header">
            <Menu width="80%" >
                    <div id="username" className="menu-item-header">
                    <Avatar name={this.state.fullName} size="60"/>
                    <span className="menu-itme-header-name">{this.state.fullName}</span>
                    </div>
                    <hr className="category-separator"/>
                    <a id="cards" className="menu-item" href={`/cards/${this.props.username}`}>Maintain Cards</a>
                    <a id="transfer" className="menu-item" href={`/transfer_money/${this.props.username}`}>Send</a>
                    <a id="load" className="menu-item"  href={`/load_wallet/${this.props.username}`}>Load Wallet</a>
                    <a id="login" className="menu-item" href={`/login`}>Sign Out</a>
            </Menu>
            <div className="logo-header">
            <img src={Logo} alt='website logo' className="logo-image-small" />
            </div>
         </div>
        )
    }
}

export default HeaderMenu