import React from 'react'
import Avatar from 'react-avatar'
import axios from 'axios';
import {CONFIG} from '../constants/config';

class TransactionDetail extends React.Component {
    constructor(props) {
        super(props);
        this.getUserTransaction(props.username);
        this.state = {
            transdata:[]
        }
    }
    getUserTransaction() {
            axios.get(`${CONFIG.apiHost}/transactions/user/${this.props.username}`).then(
                res => {
                    this.setState({transdata: res.data});
                }
            )
        }
    renderTransactionData(arr) {
        return arr.map((item, index) => {
            return (
                <div key={index} className="single-transaction-item">
                    <div className="single-transaction-item-icon"><Avatar name={item.partnerName} size="40" round="20px"/></div>
                    <div className="single-transaction-item-name">{item.partnerName}</div>
                    <div className="single-transaction-item-detail">
                        <div className={item.amount > 0 ? "single-transaction-item-positive-amount": "single-transaction-item-negative-amount"}>{this.applyMoneyFormat(item.amount)}</div>
                        <div className="single-transaction-item-date">{item.date}</div>
                        <div className="single-transaction-item-time">{item.time}</div>
                    </div>
                </div>
            )
        })
    }
    applyMoneyFormat(item) {
        if (item > 0) {
            return "+$" + item;
        }
        else {
            return "-$" + item.toString().substr(1);
        }
    }
    render() {
        return (
            <div className="transaction-detail">
                {this.renderTransactionData(this.state.transdata)}
            </div>
        )
    }
}

export default TransactionDetail