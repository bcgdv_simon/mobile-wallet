import React from 'react';
import axios from 'axios';
import {CONFIG} from '../constants/config';

class BalanceBlock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            balanceAmount: 0,
        }
        this.getUserBalance = this.getUserBalance.bind(this);
    }
    componentDidMount () {
        this.getUserBalance();
    }
    getUserBalance() {
        const URL = `${CONFIG.apiHost}/transactions/user/${this.props.username}/balance`
        axios.get(URL).then(
            res => {
                if(res !== null && res.data.balance !== undefined) {
                    this.setState({balanceAmount: res.data.balance})
                }
            }
        ).catch((error) => {throw error;})
    }
    render() {
        return (
            <div className="balance-block">
                <div className="balance-title">Current Balance</div>
                <div className="balance-detail">
                    <span className="balance-currency">$</span>
                    <span className="balance-amount">{this.state.balanceAmount}</span>
                </div>
            </div>
        )
    }
}

export default BalanceBlock