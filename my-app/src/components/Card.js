import React from 'react';
class Card extends React.Component {

    handleClick = () => this.props.onClick(this.props.index, this.props.item)

    render() {
      return  <div className="card-item">
      <div className="card-issuer">OXXO</div>
      <div className="card-number">{this.props.cardNumber}</div>
      <div className="card-type">VISA</div>
      </div>
    }
  }

export default Card